/*##################################################################
 * Seconda versione, utilizza GPS e Bussola.
 * Il drone si stabilizza usando l'MPU6050 e l'algoritmo dei PID.
 * Come input riceve un segnale ai motori tramite OSC.
 * Utilizza GPS e bussola per stare fermo nelle coordinate del decollo.
 * NON funzionante. Ancora in fase di testing dei nuovi componenti.
 ###################################################################*/



//Roll: negativo, 3 e 4, Pitch: negativo, 2,4

#include <Wire.h>
#include <Servo.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <stdio.h>
#include <string.h>
#include <OSCBundle.h>
#include <OSCBoards.h>
#include <WiFiUdp.h>
#include <SoftwareSerial.h>

SoftwareSerial swSer(D3,D4);

int val = 1000;

const char* ssid = "HUAWEI P8 lite 2017";
const char* pass = "ciaonoah123";
//const char* ssid = "Vodafone-33789299";
//const char* pass = "a24lifltjf4pdva";
WiFiClient client;
WiFiUDP udp;
byte stringa [300];

Servo esc; //1
Servo esc1; //2
Servo esc2; //4
Servo esc3; //3

float somma1 = 0;
float somma2 = 0;
float somma3 = 0;
float somma4 = 0;
boolean b=true;

int c = 1000;

int16_t gyro_x, gyro_y, gyro_z, acc_x, acc_y, acc_z, compass_x, compass_y, compass_z;;
long gyro_x_cal, gyro_y_cal, gyro_z_cal, acc_total_vector;
int temperature;
long loop_timer;
int lcd_loop_counter;
float angle_pitch, angle_roll, angle_yaw;
int angle_pitch_buffer, angle_roll_buffer;
boolean set_gyro_angles;
float angle_roll_acc, angle_pitch_acc;
float angle_pitch_output, angle_roll_output;
float errore_roll, errore_pitch, errore_yaw;
float pid_p_roll, pid_i_roll, pid_d_roll, pid_p_pitch, pid_i_pitch, pid_d_pitch, pid_p_yaw, pid_i_yaw, pid_d_yaw;
float errorePrec_roll = 0;
float errorePrec_pitch = 0;
float errorePrec_yaw = 0;
float pid_roll, pid_pitch, pid_yaw;
float angolo = 0;
float angolo_roll = 0;

float kp_roll = 2;
float ki_roll = 0.07; //0.01;
float kd_roll = 0.7; //0.5
float kp_pitch = 2;
float ki_pitch = 0.07; //0.01;
float kd_pitch = 0.5;
float kp_yaw = 1;
float ki_yaw = 0.01;
float kd_yaw = 0;
boolean fineRiga = false;
char *buf = (char *)malloc(100*sizeof(char));
char *ora = "00000000";
double Heading = 0;

TwoWire TWire = TwoWire();
//TwoWire TTWire = TwoWire();

void setup() {
  Serial.begin(2000000);
  connetti();
  TWire.begin();
  TWire.setClock(400000);
  
  setup_mpu_6050_registers();                                          
  
  for (int cal_int = 0; cal_int < 2000 ; cal_int ++){                  
    read_mpu_6050_data();                                              
    gyro_x_cal += gyro_x;                                              
    gyro_y_cal += gyro_y;                                              
    gyro_z_cal += gyro_z;                                              
    delay(3);                                                          
  }
  gyro_x_cal /= 2000;                                                  
  gyro_y_cal /= 2000;                                                  
  gyro_z_cal /= 2000;    

  setup_Compass();                                                

  esc.attach(D5); //1
  esc1.attach(D6); //2
  esc2.attach(D7); //4
  esc3.attach(D8); //3

  //Piccoli segnali per "armare" (svegliare) i motori
  while(c<=1006) {
    esc.writeMicroseconds(c);
    esc1.writeMicroseconds(c);
    esc2.writeMicroseconds(c);
    esc3.writeMicroseconds(c);
    delay(1000);
    Serial.println(c);
    c++; 
  } 
  // 

  setupGPS();
  
  loop_timer = micros();                                               
}

void loop(){

  readOrario();

  read_Compass();

  read_mpu_6050_data();                                                
  
  gyro_x -= gyro_x_cal;                                                
  gyro_y -= gyro_y_cal;                                                
  gyro_z -= gyro_z_cal;                                                
  
  //0.0000611 = 1 / (250Hz / 65.5)
  angle_pitch += gyro_x * 0.0000611;                                   
  angle_roll += gyro_y * 0.0000611;
//  angle_yaw += gyro_z * 0.0000611;   
  angle_yaw = gyro_z/65.5;                                 
  
  //0.000001066 = 0.0000611 * (3.142(PI) / 180degr)
  angle_pitch += angle_roll * sin(gyro_z * 0.000001066);               
  angle_roll -= angle_pitch * sin(gyro_z * 0.000001066);               
  
  acc_total_vector = sqrt((acc_x*acc_x)+(acc_y*acc_y)+(acc_z*acc_z));  
  //57.296 = 1 / (3.142 / 180) The Arduino asin function is in radians
  angle_pitch_acc = asin((float)acc_y/acc_total_vector)* 57.296;       
  angle_roll_acc = asin((float)acc_x/acc_total_vector)* -57.296;       
  
  angle_pitch_acc -= 0.5;                                              
  angle_roll_acc -= -3.1;                                              

  if(set_gyro_angles){                                                 
    angle_pitch = angle_pitch * 0.9996 + angle_pitch_acc * 0.0004;     
    angle_roll = angle_roll * 0.9996 + angle_roll_acc * 0.0004;        
  }
  else{                                                                
    angle_pitch = angle_pitch_acc;                                     
    angle_roll = angle_roll_acc;                                       
    set_gyro_angles = true;                                            
  }
  
  //To dampen the pitch and roll angles a complementary filter is used
  angle_pitch_output = angle_pitch_output * 0.9 + angle_pitch * 0.1;   
  angle_roll_output = angle_roll_output * 0.9 + angle_roll * 0.1;      

  //val= analogRead(A0);

  //val= map(val, 0, 1023,1000,2000);

  leggi();

  if(val>1800 || val<1000) {
    val=1000;
  }
//ROLL PID
  errore_roll = angle_roll - angolo_roll;
  pid_p_roll = kp_roll*errore_roll;
  pid_i_roll += ki_roll*errore_roll;
  pid_d_roll = ((errore_roll - errorePrec_roll)/0.004)*kd_roll;
  //pid = pid_d;
  pid_roll = pid_p_roll + pid_i_roll + pid_d_roll;
  errorePrec_roll = errore_roll;

  if(pid_roll>=100) {
    pid_roll=100;
  }
  if(pid_roll<=-100) {
    pid_roll=-100;
  }
////////////////////////////////////////
//PITCH PID
  errore_pitch = angle_pitch - angolo;
  pid_p_pitch = kp_pitch*errore_pitch;
  pid_i_pitch += ki_pitch*errore_pitch; 
  pid_d_pitch = ((errore_pitch - errorePrec_pitch)/0.004)*kd_pitch;
  //pid = pid_d;
  pid_pitch = pid_p_pitch + pid_i_pitch + pid_d_pitch;
  errorePrec_pitch = errore_pitch;

  if(pid_pitch>=100) {
    pid_pitch=100;
  }
  if(pid_pitch<=-100) {
    pid_pitch=-100;
  }
///////////////////////////////////////////////////
//PITCH yaw
  errore_yaw = (angle_yaw) - 0;
  pid_p_yaw = kp_yaw*errore_yaw;
  pid_i_yaw += ki_yaw*errore_yaw;
  pid_d_yaw = ((errore_yaw - errorePrec_yaw)/0.004)*kd_yaw;
  //pid = pid_d;
  pid_yaw = pid_p_yaw + pid_i_yaw + pid_d_yaw;
  errorePrec_yaw = errore_yaw;

  if(pid_yaw>=100) {
    pid_yaw=100;
  }
  if(pid_yaw<=-100) {
    pid_yaw=-100;
  }
///////////////////////////////////////////////////

  somma1=pid_roll + pid_pitch - pid_yaw;
  somma2=pid_roll - pid_pitch + pid_yaw;
  somma3=-pid_roll + pid_pitch + pid_yaw;
  somma4=-pid_roll - pid_pitch - pid_yaw;

  if(val<1100) {
    somma1=0;
    somma2=0;
    somma3=0;
    somma4=0;
  }
  if(val<1200) {
    pid_i_roll=0;
    pid_i_pitch=0;
    pid_i_yaw=0;
  }

  esc.writeMicroseconds(val+somma1); 
  esc1.writeMicroseconds(val+somma2); 
  esc2.writeMicroseconds(val+somma4); 
  esc3.writeMicroseconds(val+somma3); 

  Serial.print(" PITCH ");
  Serial.print(angle_pitch);
  Serial.print("  ROLL ");
  Serial.print(angle_roll);
  Serial.print(" YAW ");
  Serial.print(angle_yaw);
  Serial.print(" m1 ");
  Serial.print(somma1);
  Serial.print(" m2 ");
  Serial.print(somma2);
  Serial.print(" m3 ");
  Serial.print(somma3);
  Serial.print(" m4 ");
  Serial.print(somma4);
  Serial.print(" ANG_ROLL ");
  Serial.print(angolo_roll);
  Serial.print(" ANG_PITCH ");
  Serial.print(angolo);
  Serial.print(" ");
  Serial.print(ora);
  Serial.print(" ");
  Serial.print(Heading);
  Serial.print(" ");
  Serial.print(micros() - loop_timer);
  
  while(micros() - loop_timer < 4000);  
  Serial.print(" ");
  Serial.println(val);                              
  loop_timer = micros();                                               
}


void read_mpu_6050_data(){                                             
  TWire.beginTransmission(0x68);                                        
  TWire.write(0x3B);                                                    
  TWire.endTransmission();                                              
  TWire.requestFrom(0x68,14);                                           
  while(TWire.available() < 14);                                        
  acc_x = TWire.read()<<8|TWire.read();                                  
  acc_y = TWire.read()<<8|TWire.read();                                  
  acc_z = TWire.read()<<8|TWire.read();                                  
  temperature = TWire.read()<<8|TWire.read();                            
  gyro_x = TWire.read()<<8|TWire.read();                                 
  gyro_y = TWire.read()<<8|TWire.read();                                 
  gyro_z = TWire.read()<<8|TWire.read(); 

                                    

}

void setup_mpu_6050_registers(){
  //Activate the MPU-6050
  TWire.beginTransmission(0x68);                                        
  TWire.write(0x6B);                                                    
  TWire.write(0x00);                                                    
  int boh = TWire.endTransmission();                                    
  if(boh==0) {
    Serial.println("OK");
  } else {
    Serial.println("NO");
  }
  //Configure the accelerometer (+/-8g)
  TWire.beginTransmission(0x68);                                        
  TWire.write(0x1C);                                                    
  TWire.write(0x10);                                                    
  TWire.endTransmission();                                              
  //Configure the gyro (500dps full scale)
  TWire.beginTransmission(0x68);                                        
  TWire.write(0x1B);                                                    
  TWire.write(0x08);                                                    
  TWire.endTransmission(); 
  //Filtro passa basso 43Hz
  TWire.beginTransmission(0x68);                                        
  TWire.write(0x1A);                                                    
  TWire.write(0x03);                                                    
  TWire.endTransmission();                                            
}

void connetti() {
  delay(1000);
  Serial.println();
  Serial.print("conecting to: ");
  Serial.println(ssid);
  
  WiFi.mode(WIFI_STA); 
  delay(1000);
  WiFi.begin(ssid, pass);
  //IPAddress subnet(255,255,255,0);
  //WiFi.config(IPAddress(192,168,1,150),IPAddress(192,168,1,10),subnet);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.print("\nConnesso");

  Serial.println();
  Serial.print("My IP: ");
  Serial.println(WiFi.localIP());
  udp.begin(8000);
}

void leggi() {
  OSCMessage message;
  int size;
  if(size = udp.parsePacket() > 0) {
    udp.read(stringa, 300);
    Serial.printf("%s\n", stringa);
    message.fill(stringa, 300);
    if(!message.hasError()) {
      message.route("/drone/slider1", su);
    }
    if(!message.hasError()) {
      message.dispatch("/drone/button4", sx);
    }
    if(!message.hasError()) {
      message.dispatch("/drone/button5", dx);
    }
    if(!message.hasError()) {
      message.dispatch("/drone/button2", stopp);
    }
    if(!message.hasError()) {
      message.dispatch("/drone/button1", av);
    }
    if(!message.hasError()) {
      message.dispatch("/drone/button3", in);
    }
  }
}

void su(OSCMessage &msg, int addrOffset) {
  val = msg.getFloat(0);
//  char boh[3];
//  msg.getString(0, boh, 4);
//  Serial.println(boh);
//  int valX = atoi(boh);
//  Serial.println(valX);
}

void dx(OSCMessage &msg) {
  angolo_roll=5;
}

void sx(OSCMessage &msg) {
  angolo_roll=-5;
}

void stopp(OSCMessage &msg) {
  angolo=0;
  angolo_roll=0;
}

void av(OSCMessage &msg) {
    angolo = -5;
}

void in(OSCMessage &msg) {
    angolo = 5;
}

void setupGPS() {

  uint8_t saveConf[21] = {0xB5, 0x62, 0x06, 0x09, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x17, 0x31, 0xBF};

  //Disabilita GPGSV.
  uint8_t Disable_GPGSV[11] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x03, 0x00, 0xFD, 0x15};
  swSer.write(Disable_GPGSV, 11);

  uint8_t disable_GNGLL[11] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x01, 0x00, 0xFB, 0x11};

  swSer.write(disable_GNGLL, 11);

  uint8_t disable_GNRMC[11] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x04, 0x00, 0xFE, 0x17};

  swSer.write(disable_GNRMC, 11);

  uint8_t disable_GNVTG[11] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x05, 0x00, 0xFF, 0x19};

  swSer.write(disable_GNVTG, 11);

  uint8_t disable_GNGSA[11] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x02, 0x00, 0xFC, 0x13};

  swSer.write(disable_GNGSA, 11);
  
  uint8_t Set_to_10Hz[14] = {0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0x64, 0x00, 0x01, 0x00, 0x01, 0x00, 0x7A, 0x12};
  swSer.write(Set_to_10Hz, 14);
  
 // Baud Rate a 57k.
  uint8_t Set_to_57kbps[28] = {0xB5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0x08, 0x00, 0x00,
                               0x00, 0xE1, 0x00, 0x00, 0x07, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE2, 0xE1
                              };

uint8_t Set_to_115k[28] = {0xB5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0x08, 0x00, 0x00,
                           0x00, 0xC2, 0x01, 0x00, 0x07, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x7E
                          };
  swSer.write(Set_to_115k, 28);
  delay(200);
  //loopWrite();

  swSer.begin(115200);
  delay(200);

}

void readOrario() {
  while(swSer.available()) {
  int pos = 0;
  uint8_t lettura = swSer.read();
  if(lettura == '$') {
    while(swSer.available() && fineRiga == false) {
      lettura = swSer.read();
      buf[pos] = lettura;
      pos++;
      if(lettura == '*') fineRiga=true;
    }
    if(buf[3] == 'G' && buf[4] == 'A' && buf[6] != ',') {
//      orario1 = (buf[6] - '0')*10 + (buf[7] - '0');
//      orario2 = (buf[8] - '0')*10 + (buf[9] - '0');
//      orario3 = (buf[10] - '0')*10 + (buf[11] - '0');
//      orario4 = (buf[13] - '0')*10 + (buf[14] - '0');
//      ora = "";
//      ora = ora + buf[6] + buf[7] + buf[8] + buf[9] + buf[10] + buf[11] + buf[13] + buf[14];
      ora[0] = buf[6];
      ora[1] = buf[7];
      ora[2] = buf[8];
      ora[3] = buf[9];
      ora[4] = buf[10];
      ora[5] = buf[11];
      ora[6] = buf[13];
      ora[7] = buf[14];
    } 
  }
  fineRiga = false;
  }
}

void setup_Compass() {
  
  TWire.beginTransmission(0x1E);                     //Start communication with the compass.
  TWire.write(0x00);                                            //We want to write to the Configuration Register A (00 hex).
  TWire.write(0x78);                                            //Set the Configuration Regiser A bits as 01111000 to set sample rate (average of 8 at 75Hz).
  TWire.write(0x20);                                            //Set the Configuration Regiser B bits as 00100000 to set the gain at +/-1.3Ga.
  TWire.write(0x00);                                            //Set the Mode Regiser bits as 00000000 to set Continues-Measurement Mode.
  int ret = TWire.endTransmission();                                    
  if(ret==0) {
    Serial.println("COMPASS OK");
  } else {
    Serial.println("COMPASS KO");
  }

}

void read_Compass() {
  TWire.beginTransmission(0x1E);                     //Start communication with the compass.
  TWire.write(0x03);                                            //We want to start reading at the hexadecimal location 0x03.
  TWire.endTransmission();                                      //End the transmission with the gyro.

  TWire.requestFrom(0x1E, 6);                        //Request 6 bytes from the compass.
  compass_y = TWire.read() << 8 | TWire.read();                 //Add the low and high byte to the compass_y variable.
  compass_y *= -1;                                              //Invert the direction of the axis.
  compass_z = TWire.read() << 8 | TWire.read();                 //Add the low and high byte to the compass_z variable.;
  compass_x = TWire.read() << 8 | TWire.read();                 //Add the low and high byte to the compass_x variable.;
  compass_x *= -1;                                              //Invert the direction of the axis.

  Heading = atan2((double)compass_y, (double)compass_x);
  if (Heading>2*PI) /* Due to declination check for >360 degree */
   Heading = Heading - 2*PI;
  if (Heading<0)    /* Check for sign */
   Heading = Heading + 2*PI;
  Heading = (Heading* 180 / PI);

  //Before the compass can give accurate measurements it needs to be calibrated. At startup the compass_offset and compass_scale
  //variables are calculated. The following part will adjust the raw compas values so they can be used for the calculation of the heading.
  //if (compass_calibration_on == 0) {                            //When the compass is not beeing calibrated.
    //compass_y += compass_offset_y;                              //Add the y-offset to the raw value.
    //compass_y *= compass_scale_y;                               //Scale the y-value so it matches the other axis.
    //compass_z += compass_offset_z;                              //Add the z-offset to the raw value.
    //compass_z *= compass_scale_z;                               //Scale the z-value so it matches the other axis.
    //compass_x += compass_offset_x;                              //Add the x-offset to the raw value.
  //}

  //The compass values change when the roll and pitch angle of the quadcopter changes. That's the reason that the x and y values need to calculated for a virtual horizontal position.
  //The 0.0174533 value is phi/180 as the functions are in radians in stead of degrees.
  //compass_x_horizontal = (float)compass_x * cos(angle_pitch * -0.0174533) + (float)compass_y * sin(angle_roll * 0.0174533) * sin(angle_pitch * -0.0174533) - (float)compass_z * cos(angle_roll * 0.0174533) * sin(angle_pitch * -0.0174533);
  //compass_y_horizontal = (float)compass_y * cos(angle_roll * 0.0174533) + (float)compass_z * sin(angle_roll * 0.0174533);

  //Now that the horizontal values are known the heading can be calculated. With the following lines of code the heading is calculated in degrees.
  //Please note that the atan2 uses radians in stead of degrees. That is why the 180/3.14 is used.
  //if (compass_y_horizontal < 0)actual_compass_heading = 180 + (180 + ((atan2(compass_y_horizontal, compass_x_horizontal)) * (180 / 3.14)));
  //else actual_compass_heading = (atan2(compass_y_horizontal, compass_x_horizontal)) * (180 / 3.14);

  //actual_compass_heading += declination;                                 //Add the declination to the magnetic compass heading to get the geographic north.
  //if (actual_compass_heading < 0) actual_compass_heading += 360;         //If the compass heading becomes smaller then 0, 360 is added to keep it in the 0 till 360 degrees range.
  //else if (actual_compass_heading >= 360) actual_compass_heading -= 360; //If the compass heading becomes larger then 360, 360 is subtracted to keep it in the 0 till 360 degrees range.
}











